﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroAcademico
{
    public partial class MDIprincipal : Form
    {
        Sesion.SesionMa _SESION = Sesion.SesionMa.Instancia;
        private void CargarIndicadores()
        {
            lblUsuario.Text = _SESION.Usuario;
            lblEstacion.Text = Environment.MachineName;
        }
        public MDIprincipal()
        {
            InitializeComponent();
        }

        private void MDIprincipal_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            CargarIndicadores();
        }

        private void reportesInformesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uSUARIOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuarios frm = new frmUsuarios();
            frm.ShowDialog();
           
        }

        private void aLUMNOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mantenimientos.GUI.frmGestionAlumnos frm = new Mantenimientos.GUI.frmGestionAlumnos();
            frm.ShowDialog();
        }

        private void gRADOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGrados frm = new frmGrados();
            frm.ShowDialog();
        }

        private void sECCIONESToolStripMenuItem_Click(object sender, EventArgs e)
        {
               frmGrupos frm = new frmGrupos();
               frm.ShowDialog();
        }

        private void dOCENTESToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mantenimientos.GUI.frmDocentes frm = new Mantenimientos.GUI.frmDocentes();
            frm.ShowDialog();
        }

        private void lblUsuario_Click(object sender, EventArgs e)
        {

        }
    }
}
