﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cache;

namespace RegistroAcademico
{
    public partial class frmUsuarios : Form
    {
        BindingSource _TipoUsuario = new BindingSource();
        BindingSource _Usuarios = new BindingSource();
        
        private void Procesar()
        {
            Mantenimientos.CLS.usuarios ObjeUsu = new Mantenimientos.CLS.usuarios();
            //SINCRONIZAMOS EL OBJETO
            ObjeUsu.IDUsuarios = txtIDusuario.Text;
            ObjeUsu.Nom_usuario = txtUsuario.Text;
            ObjeUsu.Credencial = txtCredencial.Text;
            ObjeUsu.IDEmpleado = cbbTipoUsuario.SelectedValue.ToString();
            //HAY QUE DECIDIR SI ACTUALIZAR O INSERTAR
            if (txtIDusuario.TextLength > 0)
            {
                //ESTAMOS MODIFICANDO
                if (ObjeUsu.Modificar())
                {
                    MessageBox.Show("Registro actualizado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    obtenerusuarios();
                }
                else
                {
                    MessageBox.Show("Registro no fue actualizado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //ESTAMOS INSERTANDO
                if (ObjeUsu.Insertar())
                {
                    MessageBox.Show("Registro insertardo", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    obtenerusuarios();
                }
                else
                {
                    MessageBox.Show("Registro no fue insertardo", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        private void obtenerusuarios()
        {
            CacheM usuarios = new CacheM();
            _Usuarios.DataSource = usuarios.Obtener(CacheM.Consultas.USUARIOS);
            dgvUsuarios.DataSource = _Usuarios;
            dgvUsuarios.AutoGenerateColumns = false;
        }
        public frmUsuarios()
        {
            InitializeComponent();
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
           btnGuardar.Enabled = false;
           CacheM TipoUser =new CacheM();
           cbbTipoUsuario.DataSource = TipoUser.Obtener(CacheM.Consultas.EMPLEADOS);
           cbbTipoUsuario.DisplayMember = "Nombres";
           cbbTipoUsuario.ValueMember = "IDEmpleado";
           obtenerusuarios();
         
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
            txtIDusuario.Text = "";
            txtCredencial.Text = "";
            txtUsuario.Text = "";
            cbbTipoUsuario.Text = "";
        }

        private void dgvUsuarios_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btnGuardar.Enabled = true;
            if(MessageBox.Show("¿Desesa Modificar el registro?","PREGUNTA",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                
                txtUsuario.Text = dgvUsuarios.CurrentRow.Cells["NmbreUsuario"].Value.ToString();
                txtCredencial.Text = dgvUsuarios.CurrentRow.Cells["Credencial"].Value.ToString();
                txtIDusuario.Text = dgvUsuarios.CurrentRow.Cells["IDUsuario"].Value.ToString();
                cbbTipoUsuario.Text = dgvUsuarios.CurrentRow.Cells["Empleado"].Value.ToString();
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
                btnEliminar.Enabled = false;
            }
           
        }


        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Realmente desea Eliminar el Registro","PREGUNTA",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                Mantenimientos.CLS.usuarios objUsuarios = new Mantenimientos.CLS.usuarios();
                objUsuarios.IDUsuarios = dgvUsuarios.CurrentRow.Cells["IDUsuario"].Value.ToString();
                if(objUsuarios.Eliminar())
                {
                    MessageBox.Show("Registro Eliminado con Exito", "CONFIRMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    obtenerusuarios();
                }
                else
                {
                    MessageBox.Show("El Registro no fue ELiminado, Verifique", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            
        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            Procesar();
            btnAgregar.Enabled = true;
            btnModificar.Enabled = true;
            btnEliminar.Enabled = true;
            txtIDusuario.Text = "";
            txtCredencial.Text = "";
            txtUsuario.Text = "";
            cbbTipoUsuario.Text = "";
            btnGuardar.Enabled = false;
        }

    }
}
