﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroAcademico
{
    public partial class frmGrados : Form
    {
        BindingSource _Grados = new BindingSource();
        
        private void cargargrados()
        {
            Cache.CacheM Datos = new Cache.CacheM();
            _Grados.DataSource = Datos.Obtener(Cache.CacheM.Consultas.GRADOS);
            dgvGrados.AutoGenerateColumns = false;
            dgvGrados.DataSource=_Grados;
        }
        private void CargarCombo()
        {
            
            Cache.CacheM Datos = new Cache.CacheM();
            cbxPlanes.DataSource = Datos.Obtener(Cache.CacheM.Consultas.PLANES);
            cbxPlanes.DisplayMember = "Plan";
            cbxPlanes.ValueMember = "IDPlan";
           
        }
              
        private void Procesar()
        {
            Mantenimientos.CLS.Grado objgrado = new Mantenimientos.CLS.Grado();
            objgrado.IDGrado = txtCodigoGrado.Text;
            objgrado.Grados = txtGrado.Text;
            objgrado.IDPlan = cbxPlanes.SelectedValue.ToString();
            if (txtCodigoGrado.TextLength > 0)
            {
                if (objgrado.Modificar())
                {
                    MessageBox.Show("Registro actualizado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cargargrados();
                }
                else
                {
                    MessageBox.Show("Registro no fue actualizado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                if(txtGrado.TextLength==0 || cbxPlanes.Items.Count==0)
                {
                    MessageBox.Show("* Datos en Blanco intenete nuevamente", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtGrado.Focus();
                    lblAste.Visible = true;
                    lblAste1.Visible = true;
                }
                else
                { 
                    if (objgrado.Guardar())
                    {
                        MessageBox.Show("Registro insertardo", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cargargrados();
                    }
                    else
                    {
                        MessageBox.Show("Registro no fue insertardo", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        

        public frmGrados()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Procesar();
            txtCodigoGrado.Text = "";
            txtGrado.Text = "";
            cbxPlanes.Text = "";
            cargargrados();
        }

        private void frmGrados_Load(object sender, EventArgs e)
        {
            btnGuardar.Enabled = false;
            cargargrados();
            CargarCombo();
            
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Desea Modificar el Registro actual","PREGUNTA",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                btnGuardar.Enabled = true;
                btnAgregar.Enabled = false;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                txtCodigoGrado.Text = dgvGrados.CurrentRow.Cells["IDGrado"].Value.ToString();
                txtGrado.Text = dgvGrados.CurrentRow.Cells["Grado"].Value.ToString();
                cbxPlanes.Text = dgvGrados.CurrentRow.Cells["IDPlan"].Value.ToString();
                
            }
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
            btnAgregar.Enabled = true;
            btnModificar.Enabled = true;
            btnEliminar.Enabled = true;
            btnGuardar.Enabled = false;
            txtCodigoGrado.Text = "";
            txtGrado.Text = "";
            cbxPlanes.Text = "";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Realmente desea Eliminar el registro Actual?","PREGUNTA",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                Mantenimientos.CLS.Grado ObjGrados = new Mantenimientos.CLS.Grado();
                ObjGrados.IDGrado = dgvGrados.CurrentRow.Cells["IDGrado"].Value.ToString();
                if(ObjGrados.Eliminar())
                {
                    MessageBox.Show("Registro Eliminado con Exito", "CONFIRMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cargargrados();
                }
                else
                {
                    MessageBox.Show("El registro no fue Eliminado ", "CONFIRMACION", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
