﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cache;

namespace Mantenimientos.GUI
{
    public partial class frmAlumnos : Form
    {
        
        private void Procesar()
        {
            CLS.alumnos Objalumnos = new CLS.alumnos();
            Objalumnos.IDAlumno = Convert.ToInt32(txtNie.Text);
            Objalumnos.PrimerApellido = txtPrimerApellido.Text;
            Objalumnos.SegundoApellido = txtSegundoApellido.Text;
            Objalumnos.PrimerNombre = txtPrimerNombre.Text;
            Objalumnos.SegundoNombre = txtSegundoNombre.Text;
            Objalumnos.Estado = cbxEstado.Checked;
            Objalumnos.Fecha_nac = dtpFecha_nac.Text;
            Objalumnos.Fecha_ingreso = dtpFechaIngreso.Text;
            Objalumnos.Genero = cbbGenero.Text;
            if (txtCodigo.TextLength > 0)
            {
                //ESTAMOS MODIFICANDO
                if (Objalumnos.Modificar())
                {
                    MessageBox.Show("Registro actualizado", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("Registro no fue actualizado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //ESTAMOS INSERTANDO
                if (Objalumnos.Guardar())
                {
                    MessageBox.Show("Registro insertardo", "Confirmación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("Registro no fue insertardo", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        public frmAlumnos()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbxEstado_CheckedChanged(object sender, EventArgs e)
        {
            if(cbxEstado.Checked==true)
            {
                cbxEstado.Text = "Alumno Activo";
                
            }
            else
            {
                cbxEstado.Text = "Alumno Inactivo";
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmAlumnos_Load(object sender, EventArgs e)
        {
            

        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Imagenes | *.jpg;*.png;*.jpeg";
            DialogResult dglres = dlg.ShowDialog();

            if(dglres != DialogResult.Cancel)
            {
                //txtubicacion.Text = dlg.FileName;
                pictureBox1.ImageLocation = dlg.FileName;
            }
        }
    }
}
