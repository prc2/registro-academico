﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cache;


namespace Mantenimientos.GUI
{
    public partial class frmGestionAlumnos : Form
    {
        BindingSource _DATOSALUMNOS = new BindingSource();

        private void CargarAlumnos()
        {
            CacheM Datos = new CacheM();
            _DATOSALUMNOS.DataSource = Datos.Obtener(CacheM.Consultas.TODOS_LOS_AlUMNOS);
            dgvDatosAlumnos.AutoGenerateColumns = false;
            dgvDatosAlumnos.DataSource = _DATOSALUMNOS;
        }
        public frmGestionAlumnos()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmAlumnos frm = new frmAlumnos();
            frm.ShowDialog();
            CargarAlumnos();
        }

        private void frmGestionAlumnos_Load(object sender, EventArgs e)
        {
            CargarAlumnos();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            DataTable Resul = new DataTable();
            CacheM Cosulta = new CacheM();
            if(MessageBox.Show("¿Desea Modificar el Registro?","PREGUNTA",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                Cosulta.Filtro = "NIE='" + dgvDatosAlumnos.CurrentRow.Cells["NIE"].Value.ToString() + "'";
                Resul = Cosulta.Obtener(CacheM.Consultas.ALUMNOS_FILTRO);
                if(Resul.Rows.Count==1)
                {
                    frmAlumnos frm = new frmAlumnos();
                    frm.txtCodigo.Text = Resul.Rows[0]["IDAlumno"].ToString();
                    frm.txtNie.Text = Resul.Rows[0]["NIE"].ToString();
                    frm.txtPrimerApellido.Text = Resul.Rows[0]["PrimerApellido"].ToString();
                    frm.txtSegundoApellido.Text = Resul.Rows[0]["SegundoApellido"].ToString();
                    frm.txtPrimerNombre.Text = Resul.Rows[0]["PrimerNombre"].ToString();
                    frm.txtSegundoNombre.Text = Resul.Rows[0]["SegundoNombre"].ToString();
                    frm.dtpFecha_nac.Text = Resul.Rows[0]["Fecha_nac"].ToString();
                    frm.cbbGenero.Text = Resul.Rows[0]["Genero"].ToString();
                    frm.cbxEstado.Checked = Convert.ToBoolean(Resul.Rows[0]["Estado"].ToString());
                    frm.dtpFechaIngreso.Text = Resul.Rows[0]["Fecha_ingreso"].ToString();
                    frm.ShowDialog();
                    CargarAlumnos();
                }
            }
            

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Realmente desea Eliminar el Registro ?","PREGUNTA",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                CLS.alumnos objAlumnos = new CLS.alumnos();
                objAlumnos.IDAlumno = Convert.ToInt32(dgvDatosAlumnos.CurrentRow.Cells["NIE"].Value.ToString());
                if(objAlumnos.Eliminar())
                {
                    MessageBox.Show("Registro Eliminado con Exito", "CONFIRMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarAlumnos();
                }
                else
                {
                    MessageBox.Show("Registro No Fue Eliminado", "CONFIRMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
