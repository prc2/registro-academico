﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantenimientos.GUI
{
    public partial class frmMantenimientoDocentes : Form
    {
        
        private void procesar()
        {
            CLS.Docentes objDocentes = new CLS.Docentes();
            objDocentes.IDDocente = txtCodigo.Text;
            objDocentes.Nombres = txtNombres.Text;
            objDocentes.Apellidos = txtApellidos.Text;
            objDocentes.Email = txtEmail.Text;
            objDocentes.Estado = cbEstado.Checked;
            if (txtCodigo.TextLength > 0)
            {
                if (objDocentes.Modificar())
                {
                    MessageBox.Show("Registro Actualizado", "CONFIRMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("El registro no fue Actualizado", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if(txtNombres.Text=="" || txtApellidos.Text=="")
                {
                    MessageBox.Show("* Datos en Blanco Intente Nuevamente", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNombres.Focus();
                    lblasterisco.Visible = true;
                    lblasterisco1.Visible = true;
                    
                }
                else
                {
                    if (objDocentes.Guardar())
                    {
                        MessageBox.Show("Registro Guardado con Exito", "CONFIRMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("El registro no fue Guardado", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    
                }
                  
             }
        }

        public frmMantenimientoDocentes()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            procesar();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMantenimientoDocentes_Load(object sender, EventArgs e)
        {

        }

        private void cbEstado_CheckedChanged(object sender, EventArgs e)
        {
            if(cbEstado.Checked==true)
            {
                cbEstado.Text = "ACTIVO";
            }
            else
            {
                cbEstado.Text = "INACTIVO";
            }
        }

        private void txtNombres_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }
    }
}
