﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cache;

namespace Mantenimientos.GUI
{
    public partial class frmDocentes : Form
    {
        BindingSource _DOCENTES = new BindingSource();

        private void CargarDocentes()
        {
            CacheM Datos = new CacheM();
            _DOCENTES.DataSource = Datos.Obtener(CacheM.Consultas.DOCENTES);
            dgvDocentes.AutoGenerateColumns = false;
            dgvDocentes.DataSource = _DOCENTES;
        }
        public frmDocentes()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmMantenimientoDocentes frm = new frmMantenimientoDocentes();
            frm.ShowDialog();
            CargarDocentes();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Desea Modificar el Registro Actual","PREGUNTA",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                frmMantenimientoDocentes frm = new frmMantenimientoDocentes();
                frm.txtCodigo.Text = dgvDocentes.CurrentRow.Cells["IDDocente"].Value.ToString();
                frm.txtNombres.Text = dgvDocentes.CurrentRow.Cells["Nombres"].Value.ToString();
                frm.txtApellidos.Text = dgvDocentes.CurrentRow.Cells["Apellidos"].Value.ToString();
                frm.txtEmail.Text = dgvDocentes.CurrentRow.Cells["email"].Value.ToString();
                frm.cbEstado.Checked = Convert.ToBoolean(dgvDocentes.CurrentRow.Cells["Estado"].Value.ToString());
                frm.ShowDialog();
                CargarDocentes();
            }
        }

        private void frmDocentes_Load(object sender, EventArgs e)
        {
            CargarDocentes();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Realmente desea Eliminar el Registro","PREGUNTA",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                CLS.Docentes objdocentes = new CLS.Docentes();
                objdocentes.IDDocente = dgvDocentes.CurrentRow.Cells["IDDocente"].Value.ToString();
                if(objdocentes.Eliminar())
                {
                    MessageBox.Show("Registro Eliminado con Exito", "CONFIRMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDocentes();
                }
                else
                {
                    MessageBox.Show("El registro no fue Eliminado", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                   
            }
        }
    }
}
