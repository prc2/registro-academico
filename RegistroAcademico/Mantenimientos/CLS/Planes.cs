﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Planes
    {
        String _IDPlan;
        String _Plan;
        String _Detalle;

        public String Detalle
        {
            get { return _Detalle; }
            set { _Detalle = value; }
        }

        public String Plan
        {
            get { return _Plan; }
            set { _Plan = value; }
        }

        public String IDPlan
        {
            get { return _IDPlan; }
            set { _IDPlan = value; }
        }
    }
}
