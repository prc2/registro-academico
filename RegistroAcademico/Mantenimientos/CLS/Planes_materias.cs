﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Planes_materias
    {
        String _IDMaterias;
        String _Materia;
        String _IDPlan;

        public String IDPlan
        {
            get { return _IDPlan; }
            set { _IDPlan = value; }
        }

        public String Materia
        {
            get { return _Materia; }
            set { _Materia = value; }
        }

        public String IDMaterias
        {
            get { return _IDMaterias; }
            set { _IDMaterias = value; }
        }
    }
}
