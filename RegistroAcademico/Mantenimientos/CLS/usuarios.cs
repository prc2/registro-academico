﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    public class usuarios
    {
        String _IDUsuarios;
        String _Nom_usuario;
        String _Credencial;
        String _IDTipo_Usuario;

        public String IDTipo_Usuario
        {
            get { return _IDTipo_Usuario; }
            set { _IDTipo_Usuario = value; }
        }

        public String Credencial
        {
            get { return _Credencial; }
            set { _Credencial = value; }
        }

        public String Nom_usuario
        {
            get { return _Nom_usuario; }
            set { _Nom_usuario = value; }
        }

        public String IDUsuarios
        {
            get { return _IDUsuarios; }
            set { _IDUsuarios = value; }
        }
        public Boolean Insertar()
        {
            Boolean Insertado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into usuarios (Nom_usuario, Credencial, IDTipo_Usuario) values('"+_Nom_usuario+"',md5('"+_Credencial+"'),'"+_IDTipo_Usuario+"');");

            try
            {
                if (operacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Insertado = true;
                }
                else
                {
                    Insertado = false;
                }

            }
            catch
            {
                Insertado = false;
            }
            return Insertado;
        }

        public Boolean Modificar()
        {
            Boolean Modificado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update usuarios set Nom_usuario='"+_Nom_usuario+"', Credencial=md5('"+_Credencial+"'), IDTipo_Usuario='"+_IDTipo_Usuario+"' where IDUsuarios='"+_IDUsuarios+"';");

            try
            {
                if (operacion.Modificar(Sentencia.ToString()) > 0)
                {
                    Modificado = true;
                }
                else
                {
                    Modificado = false;
                }

            }
            catch
            {
                Modificado = false;
            }
            return Modificado;
        }

        public Boolean Eliminar()
        {
            Boolean Eliminado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("Delete from usuarios where IDUsuarios='" + _IDUsuarios + "';");

            try
            {
                if (operacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Eliminado = true;
                }
                else
                {
                    Eliminado = false;
                }

            }
            catch
            {
                Eliminado = false;
            }
            return Eliminado;
        }

    }
}
