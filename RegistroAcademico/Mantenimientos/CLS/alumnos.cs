﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
   public class alumnos
    {
        String _Alumno;

        public String Alumno
        {
            get { return _Alumno; }
            set { _Alumno = value; }
        }
        Int32 _IDAlumno;

        public Int32 IDAlumno
        {
            get { return _IDAlumno; }
            set { _IDAlumno = value; }
        }

        String _PrimerNombre;

        public String PrimerNombre
        {
            get { return _PrimerNombre; }
            set { _PrimerNombre = value; }
        }


        String _SegundoNombre;

        public String SegundoNombre
        {
            get { return _SegundoNombre; }
            set { _SegundoNombre = value; }
        }


        String _PrimerApellido;

        public String PrimerApellido
        {
            get { return _PrimerApellido; }
            set { _PrimerApellido = value; }
        }

        String _SegundoApellido;

        public String SegundoApellido
        {
            get { return _SegundoApellido; }
            set { _SegundoApellido = value; }
        }


        String _Fecha_nac;

        public String Fecha_nac
        {
            get { return _Fecha_nac; }
            set { _Fecha_nac = value; }
        }


        String _Genero;

        public String Genero
        {
            get { return _Genero; }
            set { _Genero = value; }
        }


        Boolean _Estado;

        public Boolean Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }


        String _Fecha_ingreso;

        public String Fecha_ingreso
        {
            get { return _Fecha_ingreso; }
            set { _Fecha_ingreso = value; }
        }

      
        //Insertar
        public Boolean Guardar()
        {
            Boolean Guardado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into alumnos(NIE, PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido,Fecha_nac,");
            Sentencia.Append(" Genero, Estado, Fecha_ingreso) values('"+_IDAlumno+"','"+_PrimerNombre+"','"+_SegundoNombre+"','");
            Sentencia.Append(" "+_PrimerApellido+"','"+_SegundoApellido+"','"+_Fecha_nac+"','"+_Genero+"','"+_Estado+"','"+_Fecha_ingreso+"');");
            try
            {
                if(operacion.Insertar(Sentencia.ToString())>0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
                
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        //Modificar
        public Boolean Modificar()
        {
            Boolean Modificado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update alumnos ");
            Sentencia.Append("set NIE='"+_IDAlumno+"',PrimerNombre='"+_PrimerNombre+"',SegundoNombre='"+_SegundoNombre+"',PrimerApellido='"+_PrimerApellido+"',SegundoApellido='"+_SegundoApellido+"',");
            Sentencia.Append("Fecha_nac='"+_Fecha_nac+"',Genero='"+_Genero+"',Estado='"+_Estado+"',Fecha_ingreso='"+_Fecha_ingreso+"' where NIE='"+IDAlumno+"';");
            try
            {
                if (operacion.Modificar(Sentencia.ToString()) > 0)
                {
                    Modificado = true;
                }
                else
                {
                    Modificado = false;
                }

            }
            catch
            {
                Modificado = false;
            }
            return Modificado;
        }
        public Boolean Eliminar()
        {
            Boolean Eliminado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("Delete from alumnos");
            Sentencia.Append(" where NIE='" + _IDAlumno + "';"); 
            try
            {
                if (operacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Eliminado = true;
                }
                else
                {
                    Eliminado = false;
                }

            }
            catch
            {
                Eliminado = false;
            }
            return Eliminado;
        }

    }
}
