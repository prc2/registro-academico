﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Grupos
    {
        String _IDGrupo;
        String _Grupo;
        String _IDGrado;
        String _Seccion;
        String _Año_lectivo;
        String _IDAula;
        String _IDDocente;

        public String IDDocente
        {
            get { return _IDDocente; }
            set { _IDDocente = value; }
        }

        public String IDAula
        {
            get { return _IDAula; }
            set { _IDAula = value; }
        }

        public String Año_lectivo
        {
            get { return _Año_lectivo; }
            set { _Año_lectivo = value; }
        }

        public String Seccion
        {
            get { return _Seccion; }
            set { _Seccion = value; }
        }

        public String IDGrado
        {
            get { return _IDGrado; }
            set { _IDGrado = value; }
        }

        public String Grupo
        {
            get { return _Grupo; }
            set { _Grupo = value; }
        }

        public String IDGrupo
        {
            get { return _IDGrupo; }
            set { _IDGrupo = value; }
        }
    }
}
