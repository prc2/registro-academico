﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Docentes
    {
        String _IDDocente;
        String _Nombres;
        String _Apellidos;
        String _email;
        Boolean _Estado;

        public Boolean Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }

        public String Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public String Apellidos
        {
            get { return _Apellidos; }
            set { _Apellidos = value; }
        }

        public String Nombres
        {
            get { return _Nombres; }
            set { _Nombres = value; }
        }

        public String IDDocente
        {
            get { return _IDDocente; }
            set { _IDDocente = value; }
        }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into docentes (Nombres, Apellidos, email, Estado) values('"+_Nombres+"','"+_Apellidos+"','"+_email+"','"+_Estado+"');");
            
            try
            {
                if (operacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }

            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Modificar()
        {
            Boolean Modificado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update docentes set Nombres='"+_Nombres+"', Apellidos='"+_Apellidos+"', email='"+_email+"', Estado='"+_Estado+"' where IDDocente='"+_IDDocente+"';");
            
            try
            {
                if (operacion.Modificar(Sentencia.ToString()) > 0)
                {
                    Modificado = true;
                }
                else
                {
                    Modificado = false;
                }

            }
            catch
            {
                Modificado = false;
            }
            return Modificado;
        }

        public Boolean Eliminar()
        {
            Boolean Eliminado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("delete from docentes where IDDocente='"+_IDDocente+"';");
            try
            {
                if (operacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Eliminado = true;
                }
                else
                {
                    Eliminado = false;
                }

            }
            catch
            {
                Eliminado = false;
            }
            return Eliminado;
        }
    }
}
