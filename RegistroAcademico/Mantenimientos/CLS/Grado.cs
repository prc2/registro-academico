﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    public class Grado
    {
        String _IDGrado;
        String _Grado;
        String _IDPlan;

        public String IDPlan
        {
            get { return _IDPlan; }
            set { _IDPlan = value; }
        }

        public String Grados
        {
            get { return _Grado; }
            set { _Grado = value; }
        }

        public String IDGrado
        {
            get { return _IDGrado; }
            set { _IDGrado = value; }
        }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into grado (Grado, planes_IDPlan1) values ('"+_Grado+"' ,'"+_IDPlan+"');");

            try
            {
                if (operacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }

            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Modificar()
        {
            Boolean Modificado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update grado set Grado='"+_Grado+"', planes_IDPlan1='"+_IDPlan+"' where IDGrado='"+_IDGrado+"';");

            try
            {
                if (operacion.Modificar(Sentencia.ToString()) > 0)
                {
                    Modificado = true;
                }
                else
                {
                    Modificado = false;
                }

            }
            catch
            {
                Modificado = false;
            }
            return Modificado;
        }

        public Boolean Eliminar()
        {
            Boolean Eliminado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("delete from grado where IDGrado='" + _IDGrado + "';");
            try
            {
                if (operacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Eliminado = true;
                }
                else
                {
                    Eliminado = false;
                }

            }
            catch
            {
                Eliminado = false;
            }
            return Eliminado;
        }
    }
}
