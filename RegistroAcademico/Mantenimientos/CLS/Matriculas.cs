﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Matriculas
    {
        String _IDMatricula;
        String _IDGrupo;
        String _IDALumno;
        String _IDUsuario;
        String _IDMaterias;
        Double _Nota1;
        Double _Nota2;
        Double _Nota3;
        Double _NotaFinal;

        public Double NotaFinal
        {
            get { return _NotaFinal; }
            set { _NotaFinal = value; }
        }

        public Double Nota3
        {
            get { return _Nota3; }
            set { _Nota3 = value; }
        }

        public Double Nota2
        {
            get { return _Nota2; }
            set { _Nota2 = value; }
        }

        public Double Nota1
        {
            get { return _Nota1; }
            set { _Nota1 = value; }
        } 

        public String IDMaterias
        {
            get { return _IDMaterias; }
            set { _IDMaterias = value; }
        }

        public String IDUsuario
        {
            get { return _IDUsuario; }
            set { _IDUsuario = value; }
        }

        public String IDALumno
        {
            get { return _IDALumno; }
            set { _IDALumno = value; }
        }

        public String IDGrupo
        {
            get { return _IDGrupo; }
            set { _IDGrupo = value; }
        }

        public String IDMatricula
        {
            get { return _IDMatricula; }
            set { _IDMatricula = value; }
        }
    }
}
