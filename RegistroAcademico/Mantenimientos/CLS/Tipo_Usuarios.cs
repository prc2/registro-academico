﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimientos.CLS
{
    class Tipo_Usuarios
    {
        String _IDTipo_Ususario;
        String _Tipo_Usuario;


        public String Tipo_Usuario
        {
            get { return _Tipo_Usuario; }
            set { _Tipo_Usuario = value; }
        }

        public String IDTipo_Ususario
        {
            get { return _IDTipo_Ususario; }
            set { _IDTipo_Ususario = value; }
        }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into tipos_usuarios (Tipo_Usuario) values('"+_Tipo_Usuario+"');");
            
            try
            {
                if (operacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }

            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Modificar()
        {
            Boolean Modificado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("update tipos_usuarios set Tipo_Usuario='" + _Tipo_Usuario + "' where IDTipo_Usuario='"+_IDTipo_Ususario+"';");

            try
            {
                if (operacion.Modificar(Sentencia.ToString()) > 0)
                {
                    Modificado = true;
                }
                else
                {
                    Modificado = false;
                }

            }
            catch
            {
                Modificado = false;
            }
            return Modificado;
        }
        public Boolean Eliminar()
        {
            Boolean Eliminado = false;
            ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("delete from tipos_usuarios where IDTipo_Usuario='" + _IDTipo_Ususario + "';");

            try
            {
                if (operacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Eliminado = true;
                }
                else
                {
                    Eliminado = false;
                }

            }
            catch
            {
                Eliminado = false;
            }
            return Eliminado;
        }
    }
}
