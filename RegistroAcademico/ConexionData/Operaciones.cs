﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionData
{
    interface Operaciones
    {
        Int32 Insertar(String psentencia);
        Int32 Modificar(String psentencia);
        Int32 Eliminar(String psentencia);
        DataTable Consultar(String pconsulta);
    }
}
