﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sesion
{
    public class SesionMa
    {
        private static volatile SesionMa _Instancia = null;
        public static SesionMa Instancia
        {
            get
            {
                Object bloqueador = new Object();
                if (_Instancia == null)
                {
                    lock (bloqueador)
                    {
                        if (_Instancia == null)
                        {
                            _Instancia = new SesionMa();
                        }
                    }
                }
                return _Instancia;
            }
        }
        String _Usuario;
        String _TipoUsuario;

        public String TipoUsuario
        {
            get { return _TipoUsuario; }
            set { _TipoUsuario = value; }
        }

        public String Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }
        private SesionMa()
        {

        }
    }
}
