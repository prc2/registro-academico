﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cache
{
    public class CacheM
    {
        String _Filtro;

        public String Filtro
        {
            get { return _Filtro; }
            set { _Filtro = value; }
        }
        public enum Consultas
        {
            INICIAR_SESION,
            TODOS_LOS_AlUMNOS,
            TIPOS_USUARIOS,
            USUARIOS,
            ALUMNOS_FILTRO,
            DOCENTES,
            PLANES,
            GRADOS,
            
        }
        public DataTable Obtener(Consultas pconsulta)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Consulta = new StringBuilder();
            switch(pconsulta)
            {
                case Consultas.INICIAR_SESION:
                    Consulta.Append("SELECT Nom_usuario, Credencial FROM usuarios");
                    Consulta.Append (" where "+_Filtro+";");
                    break;
                case Consultas.TODOS_LOS_AlUMNOS:
                    Consulta.Append("SELECT * FROM alumnos;");
                    break;
                case Consultas.TIPOS_USUARIOS:
                    Consulta.Append("SELECT * FROM tipos_usuarios;");
                    break;
                case Consultas.USUARIOS:
                    Consulta.Append("SELECT IDUsuarios, Nom_usuario, Credencial, tipo_usuario FROM usuarios u, tipos_usuarios tu where u.IDTipo_Usuario=tu.IDTipo_Usuario;");
                    break;
                case Consultas.ALUMNOS_FILTRO:
                    Consulta.Append("SELECT * FROM alumnos where "+_Filtro+";");
                    break;
                case Consultas.DOCENTES:
                    Consulta.Append("Select * from docentes;");
                    break;
                case Consultas.PLANES:
                    Consulta.Append("Select * from planes;");
                    break;
                case Consultas.GRADOS:
                    Consulta.Append("SELECT IDGrado, Grado, Plan FROM planes p, grado g where g.planes_IDPlan1=p.IDPlan; ");
                    break;
            }
            try
            {
                ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
                Resultado = operacion.Consultar(Consulta.ToString());
            }
            catch
            {

            }
            return Resultado;
        }

    }
}
